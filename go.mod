module bitbucket.org/xraynine/ra_nu
// +heroku goVersion go1.17
go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang-jwt/jwt/v4 v4.2.0
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
)

require (
	github.com/dchest/authcookie v0.0.0-20190824115100-f900d2294c8e // indirect
	github.com/dchest/passwordreset v0.0.0-20190826080013-4518b1f41006 // indirect
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838 // indirect
)
