package main

import (
	"log"

	"bitbucket.org/xraynine/ra_nu/service"
)

func main() {
	log.Println("Starting Service")
	service.StartServer()
}
