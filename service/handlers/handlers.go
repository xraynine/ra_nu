package handlers

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"strings"
	"time"

	"bitbucket.org/xraynine/ra_nu/helpers"
	"bitbucket.org/xraynine/ra_nu/service/interfaces"
	"bitbucket.org/xraynine/ra_nu/service/models"
	"bitbucket.org/xraynine/ra_nu/service/repos"
	"github.com/golang-jwt/jwt/v4"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

var pages Pages

type Pages struct {
	Login  *template.Template
	SignUp *template.Template
	Forgot *template.Template
	Reset  *template.Template
	Main   *template.Template
	Modify *template.Template
	Setup  *template.Template
}

type Handler struct {
	DB       *sql.DB
	UserRepo interfaces.UsersRepo
	JwtKey   []byte
	Claim    *models.Claims
}

func CreateNewHandler(db *sql.DB, jwtSecret string) *Handler {
	return &Handler{
		DB:       db,
		UserRepo: repos.CreateNewUserRepo(db),
		JwtKey:   []byte(jwtSecret),
	}
}

func (h *Handler) Middleware(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("token")

		if err != nil {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		tokenString := cookie.Value

		claims := &models.Claims{}

		token, err := jwt.ParseWithClaims(tokenString, claims, func(t *jwt.Token) (interface{}, error) {
			return h.JwtKey, nil
		})
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		if !token.Valid {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		h.Claim = claims

		handler.ServeHTTP(w, r)
	}
}

func (h *Handler) ValidateLoginMiddleware(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		cookie, _ := r.Cookie("token")

		if cookie != nil {
			http.Redirect(w, r, "/main", http.StatusFound)
			return
		}
		handler.ServeHTTP(w, r)
	}
}

func (handler *Handler) ServeLoginPage(w http.ResponseWriter, r *http.Request) {
	path := "static/html/login.html"

	pages.Login = template.Must(template.ParseFiles(path))
	pages.Login.Execute(w, nil)
}

func (handler *Handler) ServeSignUpPage(w http.ResponseWriter, r *http.Request) {
	path := "static/html/signup.html"

	pages.SignUp = template.Must(template.ParseFiles(path))
	pages.SignUp.Execute(w, nil)
}

func (handler *Handler) ServeSetUpPage(w http.ResponseWriter, r *http.Request) {
	path := "static/html/setup.html"

	pages.SignUp = template.Must(template.ParseFiles(path))
	pages.SignUp.Execute(w, nil)
}

func (handler *Handler) ServeForgotPage(w http.ResponseWriter, r *http.Request) {
	path := "static/html/forgot.html"

	pages.Forgot = template.Must(template.ParseFiles(path))
	pages.Forgot.Execute(w, nil)
}

func (handler *Handler) ServeResetPage(w http.ResponseWriter, r *http.Request) {
	path := "static/html/reset.html"

	pages.Reset = template.Must(template.ParseFiles(path))
	pages.Reset.Execute(w, nil)
}

func (handler *Handler) ServeMainPage(w http.ResponseWriter, r *http.Request) {
	path := "static/html/main.html"
	pages.Main = template.Must(template.ParseFiles(path))

	user, err := handler.UserRepo.GetUserById(handler.Claim.ID)
	if err != nil {
		log.Println(err)
	}

	pages.Main.Execute(w, *user)
}

func (handler *Handler) ServeModifyPage(w http.ResponseWriter, r *http.Request) {
	path := "static/html/modify.html"

	pages.Modify = template.Must(template.ParseFiles(path))
	pages.Modify.Execute(w, nil)
}

func (handler *Handler) ModifyUser(w http.ResponseWriter, r *http.Request) {

	var dto models.UpdateUser
	err := helpers.ParseRequestToModel(r, &dto)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusBadRequest, nil)
		return
	}

	err = handler.UserRepo.UpdateUser(dto)
	if err != nil {
		log.Fatal(err)
		return
	}

	helpers.EncodeResponse(w, http.StatusOK, nil)
}

func (handler *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {

	var dto models.LoginOrSignupInfo
	err := helpers.ParseRequestToModel(r, &dto)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusBadRequest, nil)
		return
	}

	response, err := handler.UserRepo.CreateUser(dto)
	if err != nil {
		log.Fatal(err)
		return
	}

	expirationTime := time.Now().Add(time.Minute * 20)

	claims := models.Claims{
		ID: int64(*response.ID),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(handler.JwtKey)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: expirationTime,
		Domain:  "127.0.0.1:8000",
		Path:    "/",
	})

	helpers.EncodeResponse(w, http.StatusOK, response)
}

func (handler *Handler) SignUpWithGoogleAuthentication(w http.ResponseWriter, r *http.Request) {
	var dto models.SignUpWithGoogleAuth
	err := helpers.ParseRequestToModel(r, &dto)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusBadRequest, nil)
		return
	}

	response, err := handler.UserRepo.CreateUserWithGoogleAuth(dto)
	if err != nil {
		log.Fatal(err)
		return
	}

	expirationTime := time.Now().Add(time.Minute * 20)

	claims := models.Claims{
		ID: int64(*response.ID),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(handler.JwtKey)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: expirationTime,
		Domain:  "127.0.0.1:8000",
		Path:    "/",
	})

	helpers.EncodeResponse(w, http.StatusOK, response)
}

func (handler *Handler) LoginWithGoogleAuthentication(w http.ResponseWriter, r *http.Request) {
	var dto models.LoginOrSignupInfo
	err := helpers.ParseRequestToModel(r, &dto)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusBadRequest, nil)
		return
	}

	response, err := handler.UserRepo.LoginUserWithGoogleAuth(dto)
	if err != nil {
		log.Fatal(err)
		return
	}

	expirationTime := time.Now().Add(time.Minute * 20)

	claims := models.Claims{
		ID: int64(*response.ID),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(handler.JwtKey)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: expirationTime,
		Domain:  "127.0.0.1:8000",
		Path:    "/",
	})

	helpers.EncodeResponse(w, http.StatusOK, response)
}

func (handler *Handler) LoginUser(w http.ResponseWriter, r *http.Request) {
	var dto models.LoginOrSignupInfo
	err := helpers.ParseRequestToModel(r, &dto)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusBadRequest, err.Error())
		return
	}

	user, err := handler.UserRepo.GetUserByEmail(dto)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	expirationTime := time.Now().Add(time.Minute * 20)

	claims := models.Claims{
		ID: int64(*user.ID),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(handler.JwtKey)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: expirationTime,
		Domain:  "127.0.0.1:8000",
		Path:    "/",
	})

	helpers.EncodeResponse(w, http.StatusOK, user)
}

func (h *Handler) GetUserByToken(w http.ResponseWriter, r *http.Request) {

	user, err := h.UserRepo.GetUserById(h.Claim.ID)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusInternalServerError, err.Error())
	}

	helpers.EncodeResponse(w, http.StatusOK, user)
}

func (h *Handler) ResetPassword(w http.ResponseWriter, r *http.Request) {
	dto := models.ResetPassword{}

	err := helpers.ParseRequestToModel(r, &dto)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusBadRequest, err.Error())
	}

	err = h.UserRepo.ResetPassword(dto)
	if err != nil {
		helpers.EncodeResponse(w, http.StatusInternalServerError, err.Error())
	}

	helpers.EncodeResponse(w, http.StatusOK, nil)
}

func (h *Handler) ForgotPassword(w http.ResponseWriter, r *http.Request) {

	email := mux.Vars(r)["email"]

	from := "raynunezespino@gmail.com"
	password := os.Getenv("PASSWORD")
	to := []string{email}
	host := "smtp.gmail.com"
	port := "587"
	address := fmt.Sprintf("%v:%v", host, port)

	user, err := h.UserRepo.GetUserByEmail(models.LoginOrSignupInfo{Email: email})
	if err != nil {
		log.Println("bobo")
		return
	}

	ps := []byte(*user.Password)

	hash, err := bcrypt.GenerateFromPassword(ps, bcrypt.DefaultCost)
	if err != nil {
		log.Println("otro bobo")
		return
	}

	err = h.UserRepo.InsertHash(string(hash))
	if err != nil {
		log.Println("Error")
		return
	}

	body := fmt.Sprintf("http://localhost:8000/reset?id=%v&email=%v", string(hash), email)
	message := []byte(fmt.Sprintf("To: %v\r\n"+
		"From: %v\r\n"+
		"Subject: Reset Password\r\n\r\n"+
		"%v", email, from, body))

	auth := smtp.PlainAuth("", from, password, host)

	err = smtp.SendMail(address, auth, from, to, message)
	if err != nil {
		log.Println(err)
		fmt.Println("error sending reset password email, err:", err)
	}
}

func (h *Handler) ValidateHash(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		err := r.ParseForm()
		if err != nil {
			http.Redirect(w, r, "/forgot", http.StatusFound)
			return
		}

		key := r.FormValue("id")
		if key == "" {
			http.Redirect(w, r, "/forgot", http.StatusFound)
			return
		}

		hash, err := h.UserRepo.ValidateHash(key)
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, "/forgot", http.StatusFound)
			return
		}

		log.Println(hash)
		log.Println(key)

		err = bcrypt.CompareHashAndPassword([]byte(strings.TrimSpace(key)), []byte(strings.TrimSpace(hash)))
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, "/forgot", http.StatusFound)
			return
		}

		handler.ServeHTTP(w, r)
	}
}
