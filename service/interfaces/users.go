package interfaces

import "bitbucket.org/xraynine/ra_nu/service/models"

type UsersRepo interface {
	GetUserById(int64) (*models.User, error)
	CreateUser(models.LoginOrSignupInfo) (*models.User, error)
	GetUserByEmail(models.LoginOrSignupInfo) (*models.User, error)
	CreateUserWithGoogleAuth(models.SignUpWithGoogleAuth) (*models.User, error)
	LoginUserWithGoogleAuth(models.LoginOrSignupInfo) (*models.User, error)
	UpdateUser(models.UpdateUser) error
	InsertHash(string) error
	ValidateHash(string) (string, error)
	ResetPassword(models.ResetPassword) error
}
