package models

import "time"

type Hash struct {
	HashPassword string    `json:"hashedpassword,omitempty"`
	ExpiresAt    time.Time `json:"expires_at,omitempty"`
	CreatedAt    time.Time `json:"created_at,omitempty"`
}
