package models

type User struct {
	ID        *int    `json:"id,omitempty"`
	Fullname  *string `json:"fullname,omitempty"`
	Address   *string `json:"address,omitempty"`
	Telephone *string `json:"telephone,omitempty"`
	Email     *string `json:"email,omitempty"`
	Password  *string `json:"password,omitempty"`
}

type LoginOrSignupInfo struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type UpdateUser struct {
	User
}

type SignedInSuccessful struct {
	User
}

type SignUpWithGoogleAuth struct {
	Fullname string `json:"fullname,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type ResetPassword struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}
