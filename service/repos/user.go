package repos

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"bitbucket.org/xraynine/ra_nu/service/models"
)

type UserRepo struct {
	DB *sql.DB
}

func CreateNewUserRepo(db *sql.DB) *UserRepo {

	return &UserRepo{
		DB: db,
	}
}

func (u *UserRepo) GetUserById(userID int64) (*models.User, error) {
	user := &models.User{}

	rows, err := u.DB.Query("SELECT * FROM users WHERE id = ?", userID)
	if err != nil {
		log.Fatal("Error while getting Users")
		return nil, nil
	}

	for rows.Next() {
		err = rows.Scan(
			&user.ID,
			&user.Fullname,
			&user.Address,
			&user.Telephone,
			&user.Email,
			&user.Password,
		)

		if err != nil {
			return nil, err
		}

	}

	return user, nil
}

func (u *UserRepo) getLastAddedUser() (*models.User, error) {
	user := &models.User{}
	rows, err := u.DB.Query("SELECT * FROM users WHERE id = (SELECT last_insert_id())")
	if err != nil {
		log.Fatal("Error while getting Users")
		return nil, nil
	}

	for rows.Next() {

		err = rows.Scan(
			&user.ID,
			&user.Fullname,
			&user.Address,
			&user.Telephone,
			&user.Email,
		)

		if err != nil {
			return nil, err
		}
	}

	return user, nil
}

func (u *UserRepo) CreateUser(dto models.LoginOrSignupInfo) (*models.User, error) {
	query := "INSERT INTO users(email, password) VALUES (?, ?)"

	_, err := u.DB.Query(query, strings.ToLower(dto.Email), dto.Password)
	if err != nil {
		return nil, fmt.Errorf(fmt.Sprintf("Error while creating user. %v", err))
	}

	user, err := u.getLastAddedUser()
	if err != nil {
		return nil, fmt.Errorf(fmt.Sprintf("Error while getting user. %v", err))
	}

	return user, nil
}

func (u *UserRepo) CreateUserWithGoogleAuth(dto models.SignUpWithGoogleAuth) (*models.User, error) {
	query := "INSERT INTO users(fullname, email, password) VALUES (?, ?, ?)"

	tx, err := u.DB.Begin()
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	_, err = tx.Query(query, dto.Fullname, strings.ToLower(dto.Email), dto.Password)
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf(fmt.Sprintf("Error while creating user. %v", err))
	}

	user, err := u.getLastAddedUser()
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf(fmt.Sprintf("Error while getting user. %v", err))
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u *UserRepo) LoginUserWithGoogleAuth(dto models.LoginOrSignupInfo) (*models.User, error) {

	user, err := u.GetUserByEmail(dto)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u *UserRepo) GetUserByEmail(dto models.LoginOrSignupInfo) (*models.User, error) {
	user := &models.User{}

	query := `SELECT * FROM users WHERE email =?`

	rows, err := u.DB.Query(query, strings.ToLower(dto.Email))
	if err != nil {
		return nil, fmt.Errorf(fmt.Sprintf("Error while getting user. %v", err))
	}

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(
			&user.ID,
			&user.Fullname,
			&user.Address,
			&user.Telephone,
			&user.Email,
			&user.Password,
		)

		if err != nil {
			return nil, fmt.Errorf("Error while scanning user.")
		}
	}

	if (models.User{}) == *user {
		return nil, fmt.Errorf("User does not exist.")
	}

	if user != nil && (user.Password != nil && dto.Password != "") && dto.Password != *user.Password {
		return nil, fmt.Errorf("Password does not match, please try again.")
	}

	return user, nil
}

func (u *UserRepo) UpdateUser(user models.UpdateUser) error {

	query := `UPDATE users SET address = ?, email = ?,
					 password = ?, fullname = ?, telephone = ?
				WHERE id = ?
			   `

	existing, err := u.GetUserById(int64(*user.ID))
	if err != nil {
		return err
	}

	existing.Address = user.Address
	existing.Email = user.Email
	existing.Password = user.Password
	existing.Fullname = user.Fullname
	existing.Telephone = user.Telephone

	log.Println(*existing.Address)
	log.Println(*existing.ID)

	tx, err := u.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(query, *existing.Address, *existing.Email, *existing.Password, *existing.Fullname, *existing.Telephone, *existing.ID)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

func (u *UserRepo) InsertHash(hash string) error {

	query := `
		INSERT INTO passwordhash(hashedpassword, expiresAt, createdAt) VALUES (?, ?, ?)
	`

	expiresAt := time.Now().Add(time.Minute * 10)
	createdAt := time.Now()

	tx, err := u.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(query, hash, expiresAt, createdAt)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil

}

func (u *UserRepo) ResetPassword(dto models.ResetPassword) error {

	tx, err := u.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	user, err := u.GetUserByEmail(models.LoginOrSignupInfo{Email: dto.Email})
	if err != nil {
		tx.Rollback()
		return err
	}

	update := models.UpdateUser{}
	update.Address = user.Address
	update.Email = user.Email
	update.Password = user.Password
	update.Fullname = user.Fullname
	update.Telephone = user.Telephone

	err = u.UpdateUser(update)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (u *UserRepo) ValidateHash(hash string) (string, error) {

	dto := &models.Hash{}

	query := `
		SELECT hashedpassword, expiresAt, createdAt FROM passwordhash WHERE hashedpassword = ?
	`

	err := u.DB.QueryRow(query, hash).Scan(&dto.HashPassword, &dto.ExpiresAt, &dto.CreatedAt)
	if err != nil {
		log.Println(err)
		return "", fmt.Errorf("Error while getting validatin hash")
	}

	log.Println(dto.HashPassword == hash)

	now := time.Now().UTC().Truncate(time.Second)

	log.Println(now)
	log.Println(dto.ExpiresAt)
	log.Println(dto.ExpiresAt.After(now))

	if dto.ExpiresAt.Before(now) {
		return "", fmt.Errorf("Link expired")
	}

	return dto.HashPassword, nil
}
