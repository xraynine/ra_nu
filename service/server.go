package service

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	"bitbucket.org/xraynine/ra_nu/service/handlers"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func StartServer() {

	port := os.Getenv("PORT")
	if port == "" {
		port = "9000"
	}

	r := mux.NewRouter()
	fs := http.FileServer(http.Dir("static/"))

	h, err := createHandler()
	if err != nil {
		log.Fatalf("An error ocurred while creating the handler. %v", err)
		return
	}

	defer h.DB.Close()

	// Web Service endpoints, so we can get the UI elements.
	r.PathPrefix("/resources/").Handler(http.StripPrefix("/resources/", fs))
	// The / endpoint will be login for now, will change it later.
	r.HandleFunc("/", h.ValidateLoginMiddleware(h.ServeLoginPage))
	r.HandleFunc("/signup", h.ValidateLoginMiddleware(h.ServeSignUpPage))
	r.HandleFunc("/main", h.Middleware(h.ServeMainPage))
	r.HandleFunc("/reset", h.ValidateHash(h.ValidateLoginMiddleware(h.ServeResetPage)))
	r.HandleFunc("/forgot", h.ValidateLoginMiddleware(h.ServeForgotPage))
	r.HandleFunc("/modify", h.Middleware(h.ServeModifyPage))

	//Database endpoints
	r.HandleFunc("/api/loginuser", h.LoginUser).Methods("POST")
	r.HandleFunc("/api/createuser", h.CreateUser).Methods("POST")
	r.HandleFunc("/api/createuserwithgoogle", h.SignUpWithGoogleAuthentication).Methods("POST")
	r.HandleFunc("/api/modifyuser", h.ModifyUser).Methods("POST")
	r.HandleFunc("/api/getuserbytoken", h.GetUserByToken).Methods("GET")
	r.HandleFunc("/api/loginwithgoogle", h.LoginWithGoogleAuthentication).Methods("POST")
	r.HandleFunc("/api/forgot/{email}", h.ForgotPassword).Methods("GET")
	r.HandleFunc("/api/resetpassword/{email}", h.ResetPassword).Methods("POST")

	log.Println(port)

	log.Panic(http.ListenAndServe(":"+port, r))
}

func createHandler() (*handlers.Handler, error) {
	DBCONN := os.Getenv("DB_CONN")
	JWTSECRET := os.Getenv("JWT_KEY")

	db, err := sql.Open("mysql", DBCONN)
	if err != nil {
		return nil, err
	}

	log.Println(DBCONN)

	return handlers.CreateNewHandler(db, JWTSECRET), nil
}
