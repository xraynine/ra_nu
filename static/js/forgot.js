var email = document.getElementById('email')
var cancelButton = document.getElementById('cancel')
var resetButton = document.getElementById('reset')
resetButton.addEventListener('click', async (event) => {
    await sendResetRequest()
})

async function sendResetRequest() {
    const resetEmail = email.value

    await fetch(`https://go-challenge-auth.herokuapp.com/api/forgot/${resetEmail}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        } ,
     })
     .then(response => alert("Please check your email inbox."))
     .catch((error) => {
         alert("There was an error with the request")
         console.log(error);
     })

}