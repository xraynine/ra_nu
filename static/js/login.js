let loggedInUser = {}

const login = document.getElementById('login')
login.addEventListener('click', async (event) => {
    event.preventDefault()
    await onSignIn()
})

async function onGoogleSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    const email = profile.getEmail()

    const payload = {
        email
    }

    await fetch('/api/loginwithgoogle', {
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        } ,
        body: JSON.stringify(payload)
     }).then(response => response.json())
     .then(data => {
         if(!data) {
             alert("No user found with Google Account. Please Sign up.")
         }
         window.location.href = "modify";
     })
     .catch((error) => {
         alert("There was an error with the request")
         console.log(error);
     })
}

async function onSignIn() {
    email = document.getElementById("email").value;
    password = document.getElementById("password").value

    if(!email || !password) {
        alert("Please enter your email or password")
        return
    }

    const payload = {
        email,
        password
    }

    const response = await fetch('/api/loginuser', {
       method: 'POST',
       mode: 'cors',
       credentials: 'include',
       headers: {
           'Content-Type': 'application/json'
       } ,
       body: JSON.stringify(payload)
    }).then(response => response.json())
    .then(data => {
        loggedInUser = data
        console.log(loggedInUser);
        window.location.href = "main"
    })
    .catch((error) => {
        alert("There was an error with the request")
        console.log(error);
    })
}   