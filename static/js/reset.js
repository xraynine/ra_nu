let passwordInput = document.getElementById('password')
let save = document.getElementById('save')
save.AddEventListener('click', async () => {
    await Save()
})
let id
let email

window.addEventListener('load', (event) => {
    id =new URLSearchParams(window.location.search).get('id')
    id =new URLSearchParams(window.location.search).get('email')
})

async function Save() {

    const password = passwordInput.value

    if (!password) {
        alert("Please, type a new password")
        return
    }

    const payload = {
        email,
        password
    }
    
        await fetch(`/api/resetpassword/`, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
         }).then(response => response.json())
         .then(data => alert("Password reseted successfully, you can close this tap now."))
         .catch((error) => {
             alert("There was an error with the request")
             console.log(error);
         })
    
}