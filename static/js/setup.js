var cancelButton = document.getElementById('cancel')
var fullName = document.getElementById('fullname')
var address = document.getElementById('address')
var telephone = document.getElementById('telephone')
var email = document.getElementById('email')
var password = document.getElementById('password')
var button = document.getElementById('save')
button.addEventListener('click', async (event) => {
    await modifyUser()
})

cancelButton.addEventListener('click', (event) => {
    window.location.href = '/main'
})

let user = {}

window.addEventListener('load', async () => {
    await fetch('/api/getuserbytoken', {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        } ,
     }).then(response => response.json())
     .then(data => {
         user = data
         fullName.value = user.fullname || ""
         address.value = user.address || ""
         telephone.value = user.telephone || ""
         email.value = user.email
         password.value = user.password
     })
     .catch((error) => {
         alert("There was an error with the request")
         console.log(error);
     })
})

async function modifyUser() {
    event.preventDefault()

    const fullname = fullName.value || ""
    const newAddress = address.value || ""
    const newTelephone = telephone.value || ""
    const newEmail = email.value
    const newEpassword = password.value

    const payload = {
        ...user,
        fullname,
        address: newAddress,
        telephone: newTelephone,
        email: newEmail,
        password: newEpassword,
    }

    await fetch('/api/modifyuser', {
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        } ,
        body: JSON.stringify(payload)
     }).then(response => response.json())
     .then(data => alert("User modified successfully"))
     .catch((error) => {
         alert("There was an error with the request")
         console.log(error);
     })

}