const signup = document.getElementById('signup')
signup.addEventListener('click', async (event) => {
    event.preventDefault()
    await onSignUp()
})

async function onGoogleSignUp(googleUser) {
    var profile = googleUser.getBasicProfile();
    const fullname = profile.getName();
    const email = profile.getEmail()
    const defaultPassword = "12345"

    const payload = {
        fullname,
        email,
        password: defaultPassword
    }

    await fetch('/api/createuserwithgoogle', {
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        } ,
        body: JSON.stringify(payload)
     }).then(response => response.json())
     .then(data => {
         window.location.href = "setup";
     })
     .catch((error) => {
         alert("There was an error with the request")
         console.log(error);
     })
}

async function onSignUp() {
    email = document.getElementById("email").value;
    password = document.getElementById("password").value

    if(!email || !password) {
        alert("Please enter your email or password")
        return
    }

    const payload = {
        email,
        password
    }

    const response = await fetch('/api/createuser', {
       method: 'POST',
       mode: 'cors',
       credentials: 'include',
       headers: {
           'Content-Type': 'application/json'
       } ,
       body: JSON.stringify(payload)
    }).then(response => response.json())
    .then(data => {
        window.location.href = "setup";
    })
    .catch((error) => {
        alert("There was an error with the request")
        console.log(error);
    })
}   